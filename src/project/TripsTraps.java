package project;

import lib.TextIO;

public class TripsTraps {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.print("Tere tulemast mängima trips-traps-trulli.\nMängu eesmärk on saada kolm oma märki ritta. \nMängija 1 on X. Mängija 2 on O.\n");
		System.out.print("Mängija 1. Vali koht kuhu panna X.\n");
		String[][] board = { { "1", "2", "3", }, { "4", "5", "6" },{ "7", "8", "9" } };
		laud(board);
		System.out.print("\n");
		kaigud1(board);
		System.out.print("Mängija 2. Vali koht kuhu panna O.\n");
		kaigud2(board);
		System.out.print("Mängija 1. Vali koht kuhu panna X.\n");
		kaigud1(board);
		System.out.print("Mängija 2. Vali koht kuhu panna O.\n");
		kaigud2(board);
		System.out.print("Mängija 1. Vali koht kuhu panna X.\n");
		kaigud1(board);
		voidukontroll(board);
		System.out.print("Mängija 2. Vali koht kuhu panna O.\n");
		kaigud2(board);
		voidukontroll(board);
		System.out.print("Mängija 1. Vali koht kuhu panna X.\n");
		kaigud1(board);
		voidukontroll(board);
		System.out.print("Mängija 2. Vali koht kuhu panna O.\n");
		kaigud2(board);
		voidukontroll(board);
		System.out.print("Mängija 1. Vali koht kuhu panna X.\n");
		kaigud1(board);
		voidukontroll(board);
		System.out.print("Tundub et tuli viik.");
		System.exit(0);
	}// main

	public static void laud(String[][] tabel) { // teeb tabeli millel mängida.
		for (int i = 0; i < tabel.length; i++) {
			for (int j = 0; j < tabel[0].length; j++) {
				System.out.printf("|"+tabel[i][j]+"|");
			}// for i
			System.out.println("");
		}// for i
	}// laud

	public static void kaigud1(String[][] kaik) {// esimese mängija käigud (x).
		int arv = TextIO.getlnInt();// mängija 1 sisestus.

		if (arv == 1) {// valib millise sisestuse juures millist käiku mängida.
			if (kaik[0][0] != "1") {// vaatab et käiku ei
															// ole juba tehtud.
				System.out.print("koht võetud\n");
				kaigud1(kaik);
			} else
				kaik[0][0] = "X";
			laud(kaik);
		} else if (arv == 2) {
			if (kaik[0][1] != "2") {
				System.out.print("koht võetud\n");
				kaigud1(kaik);
			} else
				kaik[0][1] = "X";
			laud(kaik);
		} else if (arv == 3) {
			if (kaik[0][2] != "3") {
				System.out.print("koht võetud\n");
				kaigud1(kaik);
			} else
				kaik[0][2] = "X";
			laud(kaik);
		} else if (arv == 4) {
			if (kaik[1][0] != "4") {
				System.out.print("koht võetud\n");
				kaigud1(kaik);
			} else
				kaik[1][0] = "X";
			laud(kaik);
		} else if (arv == 5) {
			if (kaik[1][1] != "5") {
				System.out.print("koht võetud\n");
				kaigud1(kaik);
			} else
				kaik[1][1] = "X";
			laud(kaik);
		} else if (arv == 6) {
			if (kaik[1][2] != "6") {
				System.out.print("koht võetud\n");
				kaigud1(kaik);
			} else
				kaik[1][2] = "X";
			laud(kaik);
		} else if (arv == 7) {
			if (kaik[2][0] != "7") {
				System.out.print("koht võetud\n");
				kaigud1(kaik);
			} else
				kaik[2][0] = "X";
			laud(kaik);
		} else if (arv == 8) {
			if (kaik[2][1] != "8") {
				System.out.print("koht võetud\n");
				kaigud1(kaik);
			} else
				kaik[2][1] = "X";
			laud(kaik);
		} else if (arv == 9) {
			if (kaik[2][2] != "9") {
				System.out.print("koht võetud\n");
				kaigud1(kaik);
			} else
				kaik[2][2] = "X";
			laud(kaik);
		} else {
			System.out.print("Vigane sisestus\n");// vigane sisestus siis kui
													// kasutaja sisestab numbri
													// mida laual ei ole
			kaigud1(kaik);

		}
		System.out.printf("\n");
	}// käigud1

	public static void kaigud2(String[][] kaik) {// teise mängija käigud (O).
		int arv = TextIO.getlnInt();// mängija 2 sisestus.

		if (arv == 1) {// valib millise sisestuse juures millist käiku mängida.
						
			if (kaik[0][0] != "1") {// vaatab et käiku ei
				System.out.print("koht võetud");         // ole juba tehtud.
				kaigud2(kaik);
			} else
				kaik[0][0] = "O";
			laud(kaik);
		} else if (arv == 2) {
			if (kaik[0][1] != "2") {
				System.out.print("koht võetud\n");
				kaigud2(kaik);
			} else
				kaik[0][1] = "O";
			laud(kaik);
		} else if (arv == 3) {
			if (kaik[0][2] != "3") {
				System.out.print("koht võetud\n");
				kaigud2(kaik);
			} else
				kaik[0][2] = "O";
			laud(kaik);
		} else if (arv == 4) {
			if (kaik[1][0] != "4") {
				System.out.print("koht võetud\n");
				kaigud2(kaik);
			} else
				kaik[1][0] = "O";
			laud(kaik);
		} else if (arv == 5) {
			if (kaik[1][1] != "5") {
				System.out.print("koht võetud\n");
				kaigud2(kaik);
			} else
				kaik[1][1] = "O";
			laud(kaik);
		} else if (arv == 6) {
			if (kaik[1][2] != "6") {
				System.out.print("koht võetud\n");
				kaigud2(kaik);
			} else
				kaik[1][2] = "O";
			laud(kaik);
		} else if (arv == 7) {
			if (kaik[2][0] != "7") {
				System.out.print("koht võetud\n");
				kaigud2(kaik);
			} else
				kaik[2][0] = "O";
			laud(kaik);
		} else if (arv == 8) {
			if (kaik[2][1] != "8") {
				System.out.print("koht võetud\n");
				kaigud2(kaik);
			} else
				kaik[2][1] = "O";
			laud(kaik);
		} else if (arv == 9) {
			if (kaik[2][2] != "9") {
				System.out.print("koht võetud\n");
				kaigud2(kaik);
			} else
				kaik[2][2] = "O";
			laud(kaik);
		} else {
			System.out.print("Vigane sisestus\n");// vigane sisestus siis kui
													// kasutaja sisestab numbri
													// mida laual ei ole.
			kaigud2(kaik);
		}
		System.out.printf("\n");
	}// käigud 2

	
	public static void voidukontroll(String[][] kaik) {// kontrollib kas võidu
														// tingimused on
														// täidetud.
		if (kaik[0][0] == "X" && kaik[0][1] == "X" && kaik[0][2] == "X") {
			System.out.print("Esimene mängija võitis.");
			System.exit(0);
		} else if (kaik[0][0] == "X" && kaik[1][1] == "X" && kaik[2][2] == "X") {
			System.out.print("Esimene mängija võitis.");
			System.exit(0);
		} else if (kaik[0][0] == "X" && kaik[1][0] == "X" && kaik[2][0] == "X") {
			System.out.print("Esimene mängija võitis.");
			System.exit(0);
		} else if (kaik[2][0] == "X" && kaik[2][1] == "X" && kaik[2][2] == "X") {
			System.out.print("Esimene mängija võitis.");
			System.exit(0);
		} else if (kaik[0][2] == "X" && kaik[1][2] == "X" && kaik[2][2] == "X") {
			System.out.print("Esimene mängija võitis.");
			System.exit(0);
		} else if (kaik[1][0] == "X" && kaik[1][1] == "X" && kaik[1][2] == "X") {
			System.out.print("Esimene mängija võitis.");
			System.exit(0);
		} else if (kaik[0][1] == "X" && kaik[1][1] == "X" && kaik[2][1] == "X") {
			System.out.print("Esimene mängija võitis.");
			System.exit(0);
		} else if (kaik[0][2] == "X" && kaik[1][1] == "X" && kaik[2][0] == "X") {
			System.out.print("Esimene mängija võitis.");
			System.exit(0);
		} else if (kaik[0][0] == "O" && kaik[0][1] == "O" && kaik[0][2] == "O") {
			System.out.print("Teine mängija võitis.");
			System.exit(0);
		} else if (kaik[0][0] == "O" && kaik[1][1] == "O" && kaik[2][2] == "O") {
			System.out.print("Teine mängija võitis.");
			System.exit(0);
		} else if (kaik[0][0] == "O" && kaik[1][0] == "O" && kaik[2][0] == "O") {
			System.out.print("Teine mängija võitis.");
			System.exit(0);
		} else if (kaik[2][0] == "O" && kaik[2][1] == "O" && kaik[2][2] == "O") {
			System.out.print("Teine mängija võitis.");
			System.exit(0);
		} else if (kaik[0][2] == "O" && kaik[1][2] == "O" && kaik[2][2] == "O") {
			System.out.print("Teine mängija võitis.");
			System.exit(0);
		} else if (kaik[1][0] == "O" && kaik[1][1] == "O" && kaik[1][2] == "O") {
			System.out.print("Teine mängija võitis.");
			System.exit(0);
		} else if (kaik[0][1] == "O" && kaik[1][1] == "O" && kaik[2][1] == "O") {
			System.out.print("Teine mängija võitis.");
			System.exit(0);
		} else if (kaik[0][2] == "O" && kaik[1][1] == "O" && kaik[2][0] == "O") {
			System.out.print("Teine mängija võitis.");
			System.exit(0);

		} else {
			return;
		}
		
	}//voidukontroll
	
	
}// TripsTraps

